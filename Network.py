import math

#Fundamental Operating Aspects
#   Input      - User selected, cannot be changed by mutation, constant
#   Bias       - Much like input but mutated by learning algorithm
#   Connection - Has input and output to and from objects, multiplies by weight, and by its nature can only have one inp
#                ut and one output furthermore reducing its computational needs
#   Neuron     - Powerhouse of the cell lol it sums inputs, goes through differentiable transfer function, and holds its
#                output for a connection.
#
#   Initially the user decides how many inputs and how many outputs there are (future maybe layers?) and the program aut
#   omatically creates a layer deep network of neurons with default values.
#
#   Then, the program runs the network, finds error percentages, and mutates the network based on this, only to run it a
#   gain in order to get closer to a better network value.



class neuron(object):
    #can have multiple lines connecting to it and multiple output lines
    #BUT, for my method, only the input needs to be minded for summation!
    connectioninput=[]
    intinput=[]

    def __init__(self):

        self.ouput = 0 #placeholder for what is to come...
        self.input = 0
        self.connectioninput=[]

    def run(self):
        self.sumimput()
        self.sigmoidrun()

    def addconnection(self,line):
        self.connectioninput.append(line)
        self.intinput = []

        for x in self.connectioninput:
            self.intinput.append(x.getoutput())


    def sumimput(self):
        self.input = sum(self.intinput)


    def sigmoidrun(self):

        temp = 1/(1+math.exp(-self.input))


        self.output = temp

    def getoutput(self):
        return self.output


    def analytics(self):
        print('------ ANALYTICAL REPORT FOR NEURON ------')
        print('Summation          : ' + str(self.input))
        print('Connections        : ' + str(len(self.connectioninput)))
        print('Connections Values : ' + str(self.intinput))
        print('Transfer           : ' + str(self.output))

class input(object):

    def __init__(self,output):
        self.output = output

    def getoutput(self):
        return self.output

class bias(object):

    #Networkwise like input but can be mutated

    def __init__(self,output):
        self.output = output

    def modify(self, value):
        self.output = value

    def getoutput(self):
        return self.output


class connection():
    #can only have a connection from and a connection to

    def __init__(self,froms,to,weight):
        self.cfrom = froms #neuron or input
        self.cto = to #neuron
        self.weight = weight

        self.run()

        #add my ass to the neuron's list!
        to.addconnection(self)
    def run(self):
        self.output = self.cfrom.getoutput() * self.weight

    def getoutput(self):
        return self.output

    def analytics(self):
        return self.output



#Create two inputs and one bias
i1 = input(0.05)
i2 = input(0.1)
b1 = bias(1)

#Create first neuron
n1 = neuron()

#Connect two inputs and the bias to first neuron
l1 = connection(i1,n1,.15)
l2 = connection(i2,n1,.2)
l3 = connection(b1,n1,.35)

n1.run()

#create 2nd neuron
n2 = neuron()

#connect first neuron to second neuron
l4 = connection(n1,n2,1.0)

n2.run()

#'TRANSFER' displays final output value of this network
n2.analytics()

#In short:
# [Input 1, Input 2, Bias 1] ---> NEURON_1 ---> NEURON_2